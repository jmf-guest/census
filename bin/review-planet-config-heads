#!/bin/sh

# Copyright 2015, 2018 Paul Wise
# Released under the MIT/Expat license, see doc/COPYING

# Review the changes to Planet Derivatives config/heads.
#
# Usage: You need to be a Debian member to run this script
# review-planet-config-heads

set -e
echo 'Downloading planet git (and making read-write), new config and new heads...'
get-planet-git planet-git
cd planet-git
if ! git config 'url.git@salsa.debian.org:.pushInsteadOf' > /dev/null ; then
	git config --local 'url.git@salsa.debian.org:.pushInsteadOf' 'https://salsa.debian.org/'
fi
cd ..
rsync --cvs-exclude --copy-unsafe-links --recursive --delete lw08.debian.org:/srv/qa.debian.org/export/derivs/census/var/planet-heads/ planet-git/heads/deriv/
rsync --cvs-exclude --copy-unsafe-links --recursive --delete lw08.debian.org:/srv/qa.debian.org/export/derivs/census/var/planet-config planet-config
compare-planet-config planet-git planet-config | patch -p0 planet-git/config/config.ini.deriv
echo 'Comparing planet config and heads...'
cd planet-git
git diff config/config.ini.deriv
git difftool --extcmd=compare-images heads/deriv/
if git diff config/config.ini.deriv | grep -q '^\+\[' ; then
	echo 'Please review the new RSS feeds:'
	git diff config/config.ini.deriv | grep '^\+\[' | sed 's_^\+\[__;s_]$__'
fi
echo 'Comparisons done, to commit the changes, please run these commands:'
echo 'cd planet-git && git commit -a && git push'
